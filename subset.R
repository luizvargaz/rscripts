# Llamar las librerias
#install.packages('readxl')
library(readxl)
#install.packages('stringr')
library(stringr)
#install.packages('stringi')
library(stringi)

# Fijar el directorio de trabajo
setwd('C:/Users/LVARGAS/Documents/CIMMYT/imputR_Python')

# Obtener las bitacoras 
caracteristicasBit <- read_excel('caracteristicasBitacora.xlsx', 1)
dim(caracteristicasBit)


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Realizar el subset de datos 
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nameArchivo <- 'EXPORTAR_kellogs_2019_12_11.xlsx'

subSet <- function(nameSheet, vector){
        
        datos <- read_excel(nameArchivo, nameSheet)
        datos <- as.data.frame(datos)
        subsetDatos <- datos[datos[,1] %in% vector,]
        dim(subsetDatos)
        
        if(!dir.exists('./sub')){dir.create('./sub')}
        
        
        nombreArchivo <- paste('./sub/', nameSheet,'.csv')
        nombreArchivo <- str_replace_all(nombreArchivo, pattern=" ", repl="")
        write.csv(subsetDatos, file = nombreArchivo, row.names = FALSE, na = '')
        print('Se ha creado el archivo')
        
}

vector <- unique(caracteristicasBit$`ID del Productor (clave for�nea)`)
subSet('03_productores', vector)

vector <- unique(caracteristicasBit$`ID de la parcela (clave for�nea)`)
subSet('04_parcelas', vector)

vector <- unique(caracteristicasBit$`ID de la bit�cora (clave primaria)`)

subSet('17_sensor Greenseeker', vector)
subSet('18_fertilizante Organico', vector)
subSet('20_riegos_Descripcion', vector)
subSet('19_riegos_Limitantes Desarrollo', vector)
subSet('22_afectaciones Cosecha_Factore', vector)
subSet('21_afectaciones Cosecha_si o no', vector)
subSet('23_residuos Cosecha', vector)
subSet('24_rendimiento', vector)
subSet('25_manejo Poscosecha_Generales', vector)
subSet('28_manejo Poscosecha_Producto', vector)
subSet('27_manejo Poscosecha_Plagas', vector)
subSet('26_manejo Poscosecha_Descripcio', vector)
subSet('29_gastos Indirectos Comerciali', vector)
subSet('30_rotacion Cultivos', vector)
subSet('02_tecnologias', vector)
subSet('31_variedades Adecuadas', vector)
subSet('07_tipo Labranza', vector)
subSet('10_aplicacion Insumos _pH', vector)
subSet('09_aplicacion Insumos _producto', vector)
subSet('08_aplicacion Insumos_descripci', vector)
subSet('11_labores Culturales&Cosecha', vector)
subSet('14_siembra Resiembra_Productos', vector)
subSet('13_siembra Resiembra_descripcio', vector)
subSet('12_siembra Resiembra_general', vector)
subSet('16_analisis Suelo_Resultado ', vector)
subSet('15_analisis Suelo_Descripcion', vector)
subSet('01_caracteristicas Bit�cora', vector)

